import numpy as np
import pylab as plt


'''
Pulse coupled spiking neural network
Python code based on pulse coupled matlab code (Izhikevich, 2003)
'''

Ne = 800
Ni = 200
re = np.random.rand(Ne)
ri = np.random.rand(Ni)
a = np.concatenate((0.02*np.ones(Ne), 0.02+0.08*ri))
b = np.concatenate((0.2*np.ones(Ne), 0.25-0.05*ri))
c = np.concatenate((-65+15*re**2, -65*np.ones(Ni)))
d = np.concatenate((8-6*re**2, 2*np.ones(Ni)))
S = np.concatenate((0.5*np.random.rand(Ne+Ni, Ne), -np.random.rand(Ne+Ni, Ni)), axis=1)
v = -65*np.ones(Ne+Ni)
u = b*v
firings = np.zeros(2)

Isyn = np.zeros(Ne+Ni)

steps = 1000
pot = np.zeros((steps))
pot2 = np.zeros((steps))
Itot = np.zeros((steps))
for t in range(steps):
    I = np.concatenate((5*np.random.randn(Ne), 2*np.random.randn(Ni)))
    I = I+Isyn
    v = v+0.5*(0.04*v**2+5*v+140-u+I)
    v = v+0.5*(0.04*v**2+5*v+140-u+I)
    pot[t] = v[100]
    u = u+a*(b*v-u)
    fired = np.where(v>=30)[0]
    spikes = np.vstack((np.ones(len(fired))*t, fired)).T
    firings= np.vstack((firings, spikes))
    v[fired] = c[fired]
    u[fired] = u[fired]+d[fired]
    Isyn = np.sum(S[:,fired], axis=1)
    pot2[t] = v[100]

plt.plot(firings[:,0], firings[:,1], '.')
plt.text(800, 1020, str(np.shape(firings)[0])+' spikes')
plt.xlabel("Time (ms)")
plt.ylabel("Neuron ID")
plt.show()
